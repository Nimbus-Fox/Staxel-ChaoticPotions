﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.ChaoticPotions.Components.Builders {
    public class ChaoticPotionsItemComponentBuilder : IItemComponentBuilder{
        public string Kind() {
            return "chaoticPotions";
        }

        public object Instance(BaseItemConfiguration item, Blob config) {
            return new ChaoticPotionsComponent(config);
        }
    }
}
