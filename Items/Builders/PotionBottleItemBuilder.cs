﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using NimbusFox.KitsuneCore.V1.Animation.Items.Renderers;
using Plukit.Base;
using Staxel.Items;

namespace NimbusFox.ChaoticPotions.Items.Builders {
    public class PotionBottleItemBuilder : IItemBuilder {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }

        public void Load() {
            Renderer = new AnimatedCraftItemRenderer();
        }
        public Item Build(Blob blob, ItemConfiguration configuration, Item spare) {
            if (spare is PotionBottleItem) {
                if (spare.Configuration != null) {
                    spare.Restore(configuration, blob);
                    return spare;
                }
            }

            var bottle = new PotionBottleItem(this, configuration);
            bottle.Restore(configuration, blob);
            return bottle;
        }

        public string Kind() {
            return KindCode;
        }

        public static string KindCode => "nimbusfox.chaoticpotions.item.potion";

        public ItemRenderer Renderer { get; set; }
    }
}
