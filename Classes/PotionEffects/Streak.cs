﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.ChaoticPotions.Interfaces;
using Plukit.Base;
using Staxel;
using Staxel.Entities;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Player;

namespace NimbusFox.ChaoticPotions.Classes.PotionEffects {
    public class Streak : IChaoticPotionEffect {
        public string Code => "nimbusfox.chaoticpotions.type.streak";
        public bool Destructive => false;
        public bool RunOnClient => false;

        public void Effect(Entity entity, EntityUniverseFacade facade) {
            var outfit = entity.Inventory.GetOutfit();

            var accessory = outfit.UnequipClothingInSlot(CharacterOutfit.ClothingSlot.Accessory);
            if (accessory != Item.NullItem) {
                ItemEntityBuilder.SpawnDroppedItem(entity, facade, new ItemStack(accessory, 1), entity.EyesPosition(),
                    new Vector3D(GameContext.RandomSource.NextDouble(0, 10), GameContext.RandomSource.NextDouble(0, 10),
                        GameContext.RandomSource.NextDouble(0, 10)),
                    entity.Logic.Heading().ToVector(), SpawnDroppedFlags.NoAutoPickup);
            }

            var hat = outfit.UnequipClothingInSlot(CharacterOutfit.ClothingSlot.Hat);
            if (hat != Item.NullItem) {
                ItemEntityBuilder.SpawnDroppedItem(entity, facade, new ItemStack(hat, 1), entity.EyesPosition(),
                    new Vector3D(GameContext.RandomSource.NextDouble(0, 10), GameContext.RandomSource.NextDouble(0, 10),
                        GameContext.RandomSource.NextDouble(0, 10)),
                    entity.Logic.Heading().ToVector(), SpawnDroppedFlags.NoAutoPickup);
            }

            var shirt = outfit.UnequipClothingInSlot(CharacterOutfit.ClothingSlot.Shirt);
            if (shirt != Item.NullItem) {
                ItemEntityBuilder.SpawnDroppedItem(entity, facade, new ItemStack(shirt, 1), entity.EyesPosition(),
                    new Vector3D(GameContext.RandomSource.NextDouble(0, 10), GameContext.RandomSource.NextDouble(0, 10),
                        GameContext.RandomSource.NextDouble(0, 10)),
                    entity.Logic.Heading().ToVector(), SpawnDroppedFlags.NoAutoPickup);
            }

            var shoes = outfit.UnequipClothingInSlot(CharacterOutfit.ClothingSlot.Shoes);
            if (shoes != Item.NullItem) {
                ItemEntityBuilder.SpawnDroppedItem(entity, facade, new ItemStack(shoes, 1), entity.EyesPosition(),
                    new Vector3D(GameContext.RandomSource.NextDouble(0, 10), GameContext.RandomSource.NextDouble(0, 10),
                        GameContext.RandomSource.NextDouble(0, 10)),
                    entity.Logic.Heading().ToVector(), SpawnDroppedFlags.NoAutoPickup);
            }

            var trousers = outfit.UnequipClothingInSlot(CharacterOutfit.ClothingSlot.Trousers);
            if (trousers != Item.NullItem) {
                ItemEntityBuilder.SpawnDroppedItem(entity, facade, new ItemStack(trousers, 1), entity.EyesPosition(),
                    new Vector3D(GameContext.RandomSource.NextDouble(0, 10), GameContext.RandomSource.NextDouble(0, 10),
                        GameContext.RandomSource.NextDouble(0, 10)),
                    entity.Logic.Heading().ToVector(), SpawnDroppedFlags.NoAutoPickup);
            }

        }
    }
}
