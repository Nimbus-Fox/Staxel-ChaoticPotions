﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.ChaoticPotions.Interfaces;
using Staxel.Logic;

namespace NimbusFox.ChaoticPotions.Classes.PotionEffects {
    public class Jeff : IChaoticPotionEffect {
        public string Code => "nimbusfox.chaoticpotions.type.jeff";
        public bool Destructive => false;
        public bool RunOnClient => false;

        public void Effect(Entity entity, EntityUniverseFacade facade) {
            entity.PlayerEntityLogic.ChangeNickname("My name Jeff", facade);

            ChaoticPotionsHook.KsCore.UserManager.MessagePlayerByEntity(entity, "nimbusfox.chaoticpotions.effect.jeff");
        }
    }
}
