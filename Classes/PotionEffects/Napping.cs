﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.ChaoticPotions.Interfaces;
using Staxel.Logic;
using Staxel.Sky;

namespace NimbusFox.ChaoticPotions.Classes.PotionEffects {
    public class Napping : IChaoticPotionEffect {
        public string Code => "nimbusfox.chaoticpotions.type.napping";
        public bool Destructive => false;
        public bool RunOnClient => false;

        public void Effect(Entity entity, EntityUniverseFacade facade) {
            facade.AdvanceTime((Staxel.Core.Constants.SundownPhase - facade.DayNightCycle().Phase) + 0.2);
            facade.AdvanceTime(Staxel.Core.Constants.DawnPhase - facade.DayNightCycle().Phase);
        }
    }
}
