﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using NimbusFox.ChaoticPotions.Interfaces;
using Staxel.Logic;

namespace NimbusFox.ChaoticPotions.Classes.PotionEffects {
    public class Random : IChaoticPotionEffect {
        public string Code => "nimbusfox.chaoticpotions.type.random";
        public bool Destructive => _randomEffect.Destructive;
        public bool RunOnClient => _randomEffect.RunOnClient;
        public string EffectCode => _randomEffect.Code;

        private IChaoticPotionEffect _randomEffect;

        public Random() {
            _randomEffect = ChaoticPotions.GetRandom();
        }

        public void Effect(Entity entity, EntityUniverseFacade facade) {
            _randomEffect.Effect(entity, facade);
            _randomEffect = ChaoticPotions.GetRandom();
        }
    }
}
