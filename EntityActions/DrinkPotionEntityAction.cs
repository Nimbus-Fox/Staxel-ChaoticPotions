﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.ChaoticPotions.Items;
using Plukit.Base;
using Staxel;
using Staxel.Effects;
using Staxel.EntityActions;
using Staxel.Items;
using Staxel.Items.ItemComponents;
using Staxel.Logic;
using Staxel.Player;

namespace NimbusFox.ChaoticPotions.EntityActions {
    class DrinkPotionEntityAction : EntityActionDriver {
        public DrinkPotionEntityAction() {
            this.RegisterState("Begin", new Action<Entity, EntityUniverseFacade>(this.LoopAnim), true, (Action<Entity, EntityUniverseFacade>)null, false);
            this.RegisterState("Loop", new Action<Entity, EntityUniverseFacade>(this.Loop), true, new Action<Entity, EntityUniverseFacade>(this.LoopUpdate), false);
            this.RegisterState("End", new Action<Entity, EntityUniverseFacade>(this.End), true, (Action<Entity, EntityUniverseFacade>)null, false);
            this.RegisterState("EndFail", new Action<Entity, EntityUniverseFacade>(this.EndFail), true, (Action<Entity, EntityUniverseFacade>)null, false);
        }

        public void LoopAnim(Entity entity, EntityUniverseFacade facade) {
            this.RunAnimation(entity, "staxel.emote.Drink.loop", "Loop");
            ConsumableComponent component;
            if (!TryGetConsumableComponent(entity, out component) || string.IsNullOrEmpty(component.Sound))
                return;
            BaseEffects.PlaySound(entity, component.Sound, -1.0);
        }

        public void LoopUpdate(Entity entity, EntityUniverseFacade facade) {
            if (!entity.Controller.ControlState(ControlAxis.Main).Down) {
                this.RunAnimation(entity, "staxel.emote.Eat.end", "EndFail");
            } else {
                ConsumableComponent component;
                if (!TryGetConsumableComponent(entity, out component))
                    return;
                Vector3D vector = entity.Logic.Heading().ToVector();
                BaseEffects.BreakTileEffect(entity, "", component.Particles, entity.Physics.Position + vector, component.Colour);
            }
        }

        private static bool TryGetConsumableComponent(Entity entity, out ConsumableComponent component) {
            component = (ConsumableComponent)null;
            CraftItem craftItem = entity.Inventory.ActiveItem().Item as CraftItem;
            if (craftItem == null)
                return false;
            CraftItem edible = craftItem.FindEdible();
            if (edible == null)
                return false;
            component = edible.Configuration.Components.GetOrDefault<ConsumableComponent>();
            return component != null;
        }

        public void Loop(Entity entity, EntityUniverseFacade facade) {
            int num = (int)entity.Logic.ActionFacade.GetActionCookie("charge", 0L) + 1;
            entity.Logic.ActionFacade.SetActionCookie("charge", (long)num);
            if (!(entity.Inventory.ActiveItem().Item is PotionBottleItem craftItem)) {
                this.RunAnimation(entity, "staxel.emote.Drink.end", "EndFail");
            } else {
                if (!entity.Controller.ControlState(ControlAxis.Main).Down)
                    this.RunAnimation(entity, "staxel.emote.Drink.end", "EndFail");
                else if ((long)num >= (long)craftItem.Configuration.EatTime) {
                    PlayerEntityLogic playerEntityLogic = entity.PlayerEntityLogic;
                    if (!entity.PlayerEntityLogic.CreativeModeEnabled()) {
                        playerEntityLogic.ItemFacade.ConsumeOneActiveItemAndProduceItem(new ItemStack(), facade);
                    }
                    this.RunAnimation(entity, "staxel.emote.Drink.end", "End");
                    ChaoticPotionsHook.EffectQueue.Enqueue((entity.Id, craftItem.Effect));
                } else
                    this.LoopAnim(entity, facade);
            }
        }

        public void EndFail(Entity entity, EntityUniverseFacade facade) {
            entity.Logic.ActionFacade.NoNextAction();
            this.OnCancel(entity);
        }

        public void End(Entity entity, EntityUniverseFacade facade) {
            entity.Logic.ActionFacade.NoNextAction();
            this.OnCancel(entity);
        }

        protected override void OnCancel(Entity entity) {
            base.OnCancel(entity);
            entity.Logic.ActionFacade.SetActionCookie("charge", -1L);
        }

        public override string Kind() {
            return KindCode();
        }

        public static string KindCode() {
            return "nimbusfox.entityAction.drinkPotion";
        }

        public override void Start(Entity entity, EntityUniverseFacade facade) {
            this.RunAnimation(entity, "staxel.emote.Drink.begin", "Begin");
            entity.Logic.ActionFacade.SetActionCookie("charge", 0L);
        }
    }
}