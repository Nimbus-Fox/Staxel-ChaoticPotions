﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using NimbusFox.ChaoticPotions.Interfaces;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;

namespace NimbusFox.ChaoticPotions.Classes.PotionEffects {
    public class Teleport : IChaoticPotionEffect {
        public string Code => "nimbusfox.chaoticpotions.type.teleport";
        public bool Destructive => false;
        public bool RunOnClient => false;

        public void Effect(Entity entity, EntityUniverseFacade facade) {
            var pos = entity.FeetLocation();

            var targetPos = new Vector3D(
                GameContext.RandomSource.NextDouble(pos.X - 100, pos.X + 100),
                GameContext.RandomSource.NextDouble(pos.Y - 100, pos.Y + 100),
                GameContext.RandomSource.NextDouble(pos.Z - 100, pos.Z + 100)
            );

            var tilePos = new Vector3I(
                (int)Math.Floor(targetPos.X),
                (int)Math.Floor(targetPos.Y),
                (int)Math.Floor(targetPos.Z)
            );

            var canTeleport = false;

            var loopLimit = new LoopLimiter(5000, 20);

            while (!canTeleport || loopLimit.Check()) {
                if (facade.ReadTile(tilePos, TileAccessFlags.SynchronousWait, ChunkFetchKind.LivingWorld, EntityId.NullEntityId, out var tile)) {
                    if (tile.Configuration.Code == Staxel.Core.Constants.SkyCode) {
                        canTeleport = true;
                    } else {
                        targetPos = new Vector3D(
                            GameContext.RandomSource.NextDouble(pos.X - 100, pos.X + 100),
                            GameContext.RandomSource.NextDouble(pos.Y - 100, pos.Y + 100),
                            GameContext.RandomSource.NextDouble(pos.Z - 100, pos.Z + 100)
                        );

                        tilePos = new Vector3I(
                            (int)Math.Floor(targetPos.X),
                            (int)Math.Floor(targetPos.Y),
                            (int)Math.Floor(targetPos.Z)
                        );
                    }
                }
            }

            entity.Physics.Teleport(targetPos);
            entity.Physics.Velocity = new Vector3D(0, 0, 0);
            entity.Physics.NeedsStorage();
        }
    }
}
