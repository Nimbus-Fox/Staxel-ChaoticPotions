﻿using System.Collections.Generic;
using System.Linq;
using NimbusFox.ChaoticPotions.Components;
using NimbusFox.ChaoticPotions.EntityActions;
using NimbusFox.ChaoticPotions.Interfaces;
using NimbusFox.ChaoticPotions.Items.Builders;
using NimbusFox.KitsuneCore.V1.Animation.Classes;
using NimbusFox.KitsuneCore.V1.Animation.Components;
using NimbusFox.KitsuneCore.V1.Animation.Interfaces;
using NimbusFox.KitsuneCore.V1.Animation.Items;
using Plukit.Base;
using Staxel.Client;
using Staxel.Collections;
using Staxel.Draw;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Tiles;

namespace NimbusFox.ChaoticPotions.Items {
    public class PotionBottleItem : Item, IAnimatedItem {

        private readonly PotionBottleItemBuilder _builder;
        
        public IChaoticPotionEffect Effect { get; }

        public PotionBottleItem(PotionBottleItemBuilder builder, ItemConfiguration configuration) : base(builder.Kind()) {
            _builder = builder;
            Configuration = configuration;

            var component = configuration.Components.Select<ChaoticPotionsComponent>().FirstOrDefault();

            Effect = ChaoticPotions.GetEffect(component == default(ChaoticPotionsComponent) ? "nimbusfox.chaoticpotions.type.random" : component.Type);

            if (configuration.Components.Contains<KsAnimationComponent>()) {
                AnimationComponent = configuration.Components.Get<KsAnimationComponent>();
            }
        }

        public override void Update(Entity entity, Timestep step, EntityUniverseFacade entityUniverseFacade) { }

        public override void Control(Entity entity, EntityUniverseFacade facade, ControlState main, ControlState alt) {
            if (main.DownClick) {
                entity.Logic.ActionFacade.NextAction(DrinkPotionEntityAction.KindCode());
            }
        }

        protected override void AssignFrom(Item item) { }

        public override ItemRenderer FetchRenderer() {
            return _builder.Renderer;
        }

        public override bool PlacementTilePreview(AvatarController avatar, Entity entity, Universe universe, Vector3IMap<Tile> previews) {
            return false;
        }

        public override bool HasAssociatedToolComponent(Plukit.Base.Components components) {
            return false;
        }

        public KsAnimationComponent AnimationComponent { get; }
        public MultipleMatrixDrawable IconDrawable { get; set; }
        public MultipleMatrixDrawable CompactDrawable { get; set; }
        public MultipleMatrixDrawable SecondaryDrawable { get; set; }
    }
}
