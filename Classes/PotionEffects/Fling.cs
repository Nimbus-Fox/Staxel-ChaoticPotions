﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.ChaoticPotions.Interfaces;
using Plukit.Base;
using Staxel;
using Staxel.Logic;

namespace NimbusFox.ChaoticPotions.Classes.PotionEffects {
    public class Fling : IChaoticPotionEffect {
        public string Code => "nimbusfox.chaoticpotions.type.fling";
        public bool Destructive => false;
        public bool RunOnClient => false;

        public void Effect(Entity entity, EntityUniverseFacade facade) {
            entity.Physics.AddForce(new Vector3D(0,
                Staxel.Core.Constants.PlayerJumpForce * GameContext.RandomSource.Next(10, 1000), 0));
            entity.Physics.NeedsStorage();
        }
    }
}
