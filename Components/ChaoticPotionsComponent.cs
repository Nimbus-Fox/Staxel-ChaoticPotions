﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plukit.Base;

namespace NimbusFox.ChaoticPotions.Components {
    public class ChaoticPotionsComponent {
        public string Type { get; }
        public ChaoticPotionsComponent(Blob config) {
            Type = config.GetString("type");
        }
    }
}
