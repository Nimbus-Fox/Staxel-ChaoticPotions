﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using NimbusFox.ChaoticPotions.Components;
using NimbusFox.ChaoticPotions.Interfaces;
using NimbusFox.KitsuneCore.V1;
using NimbusFox.KitsuneCore.V1.Components;
using Plukit.Base;
using Staxel;

namespace NimbusFox.ChaoticPotions {
    public static class ChaoticPotions {
        private static readonly List<IChaoticPotionEffect> PotionEffects = new List<IChaoticPotionEffect>();

        internal static void InitializeDatabase() {
            PotionEffects.Clear();
            var types = Helpers.GetTypesUsingBase<IChaoticPotionEffect>();

            foreach (var type in types) {
                var current = (IChaoticPotionEffect)Activator.CreateInstance(type);

                PotionEffects.Add(current);
            }

            Logger.WriteLine($"[Chaotic Potions] {PotionEffects.Count} potion effects loaded");
        }

        public static IChaoticPotionEffect GetEffect(string code) {
            return PotionEffects.FirstOrDefault(x => x.Code == code);
        }

        public static IChaoticPotionEffect GetRandom() {
            return GameContext.RandomSource.Pick(PotionEffects
                .Where(x => x.Code != "nimbusfox.chaoticpotions.type.random").ToList());
        }
    }
}
