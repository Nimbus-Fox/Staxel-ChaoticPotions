﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.ChaoticPotions.Interfaces;
using NimbusFox.KitsuneCore.Interfaces;
using NimbusFox.KitsuneCore.V1;
using Plukit.Base;
using Staxel;
using Staxel.Items;
using Staxel.Logic;
using Staxel.Modding;
using Staxel.Player;
using Staxel.Tiles;

namespace NimbusFox.ChaoticPotions {
    public class ChaoticPotionsHook : ITailModHookV3 {
        /// <summary>Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.</summary>
        public void Dispose() { }
        public void GameContextInitializeInit() { }
        public void GameContextInitializeBefore() { }

        internal static KitsuneCore.V1.KitsuneCore KsCore = new KitsuneCore.V1.KitsuneCore("NimbusFox", "Chaotic Potions");

        internal static readonly Queue<(EntityId entity, IChaoticPotionEffect effect)> EffectQueue = new Queue<(EntityId entity, IChaoticPotionEffect effect)>();

        private static readonly Dictionary<EntityId, string> SendToClients = new Dictionary<EntityId, string>();

        public void GameContextInitializeAfter() {
            ChaoticPotions.InitializeDatabase();
        }
        public void GameContextDeinitialize() { }
        public void GameContextReloadBefore() { }
        public void GameContextReloadAfter() { }

        public void UniverseUpdateBefore(Universe universe, Timestep step) {
            if (Helpers.IsServer()) {
                universe.SetCheated();
            }
            if (EffectQueue.TryDequeue(out var item)) {
                if (universe.TryGetEntity(item.entity, out var entity)) {
                    if (!item.effect.RunOnClient) {
                        item.effect.Effect(entity, universe);
                    } else if (Helpers.IsClient()) {
                        if (ClientContext.PlayerFacade.IsLocalPlayer(entity)) {
                            item.effect.Effect(entity, universe);
                        }
                    } else {
                        SendToClients.Add(item.entity.Id, item.effect.Code);
                    }
                }
            }
        }

        public void UniverseUpdateAfter() {

        }

        public bool CanPlaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanReplaceTile(Entity entity, Vector3I location, Tile tile, TileAccessFlags accessFlags) {
            return true;
        }

        public bool CanRemoveTile(Entity entity, Vector3I location, TileAccessFlags accessFlags) {
            return true;
        }

        public void ClientContextInitializeInit() { }
        public void ClientContextInitializeBefore() { }
        public void ClientContextInitializeAfter() {
        }
        public void ClientContextDeinitialize() { }
        public void ClientContextReloadBefore() { }
        public void ClientContextReloadAfter() { }
        public void CleanupOldSession() { }
        public bool CanInteractWithTile(Entity entity, Vector3F location, Tile tile) {
            return true;
        }

        public bool CanInteractWithEntity(Entity entity, Entity lookingAtEntity) {
            return true;
        }

        public void OnPlayerLoadAfter(Blob blob) { }
        public void OnPlayerSaveBefore(PlayerEntityLogic logic, out Blob saveBlob) {
            saveBlob = null;
        }

        public void OnPlayerSaveAfter(PlayerEntityLogic logic, out Blob saveBlob) {
            saveBlob = null;
        }

        public void OnPlayerConnect(Entity entity) { }
        public void OnPlayerDisconnect(Entity entity) { }

        public void Store(Blob blob) {
            foreach (var item in SendToClients) {
                blob.SetString(item.Key.Id.ToString(), item.Value);
            }

            SendToClients.Clear();
        }

        public void Restore(Blob blob) {
            foreach (var item in blob.KeyValueIteratable) {
                if (item.Value.Kind != BlobEntryKind.String) {
                    continue;
                }

                if (long.TryParse(item.Key, out var id)) {
                    var effect = ChaoticPotions.GetEffect(item.Value.GetString());
                    if (effect != null) {
                        EffectQueue.Enqueue((id, effect));
                    }
                }
            }
        }
    }
}
