﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NimbusFox.ChaoticPotions.Interfaces;
using Staxel;
using Staxel.Logic;

namespace NimbusFox.ChaoticPotions.Classes.PotionEffects {
    public class RagsOrRiches : IChaoticPotionEffect {
        public string Code => "nimbusfox.chaoticpotions.type.ragsOrRiches";
        public bool Destructive => false;
        public bool RunOnClient => false;

        public void Effect(Entity entity, EntityUniverseFacade facade) {
            var chance = GameContext.RandomSource.Next(0, 100);

            if (chance <= 20) {
                var give = GameContext.RandomSource.Next(1000, int.MaxValue - (entity.Inventory.GetMoney() * 20));
                entity.Inventory.ResetMoney(entity.Inventory.GetMoney() + give);
                ChaoticPotionsHook.KsCore.UserManager.MessagePlayerByEntity(entity, "nimbusfox.chaoticpotions.effect.riches", $"{give:N0}");
            } else {
                entity.Inventory.ResetMoney(0);
                ChaoticPotionsHook.KsCore.UserManager.MessagePlayerByEntity(entity, "nimbusfox.chaoticpotions.effect.rags");
            }
        }
    }
}
