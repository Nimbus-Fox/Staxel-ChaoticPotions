﻿using Microsoft.Xna.Framework;
using Staxel.Logic;

namespace NimbusFox.ChaoticPotions.Interfaces {
    public interface IChaoticPotionEffect {
        string Code { get; }
        bool Destructive { get; }
        bool RunOnClient { get; }
        void Effect(Entity entity, EntityUniverseFacade facade);
    }
}